from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


# Create your views here.
def todoList(req):
    todos = TodoList.objects.all()
    context = {
        "todoList": todos,
    }
    return render(req, "todos/list.html", context)


def todoDetail(req, id):
    item = get_object_or_404(TodoList, id=id)
    context = {
        "item": item,
    }
    return render(req, "todos/todoDetail.html/", context)
